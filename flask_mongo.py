from flask import Flask, request
from flask_restful import Api, Resource
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo

app = Flask(__name__)
api = Api(app)
app.config['MONGO_URI'] = 'mongodb://localhost:27017/drinksdb'

mongo = PyMongo(app)

class BeerApi(Resource):
    def get(self):
        beer = mongo.db.beer
        output = []
        for s in beer.find():
          output.append({'name' : s['name'], 'type' : s['type']})
        return jsonify({'result' : output})

    def post(self):
        beer = mongo.db.beer
        args = request.json
        beer.insert_one(args)
        return "", 201

class MainApi(Resource):
    def get(self):
        return "",200

api.add_resource(BeerApi, "/beers")
api.add_resource(MainApi, "/")
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
